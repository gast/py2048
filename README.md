2048-python
===========

Based on the popular game [2048](https://github.com/gabrielecirulli/2048) by Gabriele Cirulli, here is a Python version that uses TKinter. 

![screenshot](img/screenshot.png)

To start the game, run:
    
    $ python main2048.py

For more options, use the --help option. 


Contributors:
==

Forked from https://github.com/yangshun/2048-python

- [Nicolas Gast](http://mescal.imag.fr/membres/nicolas.gast/)
- [Tay Yang Shun](http://github.com/yangshun)
- [Emmanuel Goh](http://github.com/emman27)