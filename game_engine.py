"""
This class contains all functions needed to create a new Game of 2048
"""
from random import randrange
import numpy as np

class GameEngine:
    """
    This class contains all functions needed to play a new Game of 2048
    """
    def __init__(self, size=4, verbose=0):
        """
        args:
        - size = int (size of the grid)
        - verbose = int (default 0 = print nothing)
        """
        self.size = size # Size of the game
        self.board_sequences = None
        self.board = None
        self.number_of_generated_values = None
        self.verbose = verbose
        self.new_game()

    def new_game(self):
        """
        This creates an empty board and generates two new numbers
        """
        self.board = self.empty_game() # State of the game
        self.number_of_generated_values = 0
        self.generate_new_number()
        self.generate_new_number()
        self.board_sequences = []

    def run_game(self, strategy):
        """
        Run a game and returns the sequence of board games
        """
        board_sequence = []
        while self.game_state() not in ['win', 'lose']:
            self.print()
            if self.play(strategy(self.board)):
                # If this is a valid action, we store the current board
                board_sequence.append(self.board)
        self.print()
        return np.array(board_sequence)

    def run_new_game(self, strategy):
        """
        Creates a new game and launch the game until the end.
        """
        self.new_game()
        return self.run_game(strategy)

    def empty_game(self):
        """
        Return an empty game (of size self.size)
        """
        return [[0]*self.size for i in range(self.size)]


    def generate_new_number(self):
        """
        This function generates a new random number.
        The new number is uniformly distributed among the empty spaces.
        (it assumes that there exists an empty space)
        """
        i = randrange(0, self.size)
        j = randrange(0, self.size)
        while self.board[i][j] != 0:
            i = randrange(0, self.size)
            j = randrange(0, self.size)
        self.board[i][j] = 2 if np.random.rand() <= 0.9 else 4
        self.number_of_generated_values += 1

    def game_state(self):
        """
        Computes if the game is finished or not.
        Return:
        - 'win'
        - 'free' [if there is an empty space but the game is not won]
        - 'not over' [if we can still play even if there is no free space]
        - 'lose'
        """
        has_free_space = False
        for i in range(self.size):
            for j in range(self.size):
                if self.board[i][j] == 2048:
                    return 'win'
                if self.board[i][j] == 0:
                    has_free_space = True
        if has_free_space:
            return 'free'
        for i in range(self.size):
            for j in range(self.size):
                if (i+1 < self.size and self.board[i][j] == self.board[i+1][j] or
                        j+1 < self.size and self.board[i][j+1] == self.board[i][j]):
                    return 'not over'
        return 'lose'

    def reverse(self, mat):
        """
        Return a mirror version of a matrix
        """
        return [[mat[i][self.size-j-1] for j in range(self.size)] for i in range(self.size)]

    def transpose(self, mat):
        """
        Return a transposed version of a matrix
        """
        return [[mat[j][i] for j in range(self.size)] for i in range(self.size)]

    def compress(self, mat):
        """
        Compress the matrix from top to bottom
        """
        new = self.empty_game()
        for i in range(self.size):
            count = 0
            for j in range(self.size):
                if mat[i][j] != 0:
                    new[i][count] = mat[i][j]
                    count += 1
        return new

    def merge(self, mat):
        """
        Sum two numbers if two consecutive numbers are equal (from top to bottom)
        """
        for i in range(self.size):
            for j in range(self.size-1):
                if mat[i][j] == mat[i][j+1] and mat[i][j] != 0:
                    mat[i][j] *= 2
                    mat[i][j+1] = 0
        return mat

    def compress_and_merge(self, mat):
        """
        The function is implemented for a 'down' merge.
        This modifies the game in place.
        """
        game = self.compress(mat)
        game = self.merge(game)
        game = self.compress(game)
        return game

    def transpose_action(self, game, action_number):
        """
        This function returns a transposed version of the game
        (to be used before and after compress_and_merge).

        This function is here because compress_and_merge is only implemented for 'down' merge
        """
        if action_number == 0:
            return self.transpose(game)
        if action_number == 1:
            return self.reverse(self.transpose(game))
        if action_number == 2:
            return game
        if action_number == 3:
            return self.reverse(game)
        if action_number == 4:
            return self.transpose(self.reverse(game)) # Hack pas terrible
        return None

    def is_action_valid(self, action_number):
        """
        Returns "True" if action_number is succesful and "False" overwise
        """
        game_old = self.transpose_action(self.board, action_number)
        game_new = self.compress_and_merge(game_old)
        return game_old != game_new

    def apply_action(self, action_number):
        """
        Tries to apply the action. return "True" if the move is succesful
        """
        if self.verbose >= 1:
            print(GameEngine.name_action(action_number))
        game_old = self.transpose_action(self.board, action_number)
        game_new = self.compress_and_merge(game_old)
        self.board = self.transpose_action(game_new, (action_number if action_number != 1 else 4))
        return game_old != game_new

    def play(self, action_number):
        """
        Tries to apply the action "action_number" and generates a new number
        if this action is successful.
        """
        if self.apply_action(action_number):
            self.generate_new_number()
            return True
        return False

    def print(self):
        """
        Print the current state of the game
        """
        if self.verbose == 0:
            return
        print('#### turn', self.number_of_generated_values, "####", self.game_state())
        for i in range(self.size):
            print(self.board[i])

    def list_of_next_random(self, board):
        """
        Returns a list of board that could be the next possible boards after
        a random moves (and the correspondinng probabilities).

        This function does not modify the current board.
        """
        nexts = []
        probas = []
        for i in range(self.size):
            for j in range(self.size):
                if board[i][j] == 0:
                    for next_value in [2, 4]:
                        new_board = np.copy(board)
                        new_board[i, j] = next_value
                        nexts.append(new_board)
                        probas.append(0.9 if next_value == 2 else 0.1)
        return nexts, [p/sum(probas) for p in probas]

    def list_of_next_choices(self, board):
        """
        Returns a list of boards that could be the next possible boards after an action

        This function does not modify the current board.
        """
        saved_board = self.board
        nexts = []
        for action in [0, 1, 2, 3]:
            self.board = board
            if self.apply_action(action):
                nexts.append(self.board)
        self.board = saved_board
        return nexts

    @staticmethod
    def name_action(action_number):
        """
        Returns a string corresponding to the name of the action.
        """
        return ['up', 'down', 'left', ' right'][action_number]
