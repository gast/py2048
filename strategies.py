"""
This file defines the various strategies that can be used.
"""
from random import randrange

class Strategies:
    """
    Contains various strategies for the 2048 game
    """

    @staticmethod
    def command_line_action():
        """
        Prompt the user on the command line to get the action
        """
        action = int(input("Indicate an action between 0 and 3 (0=Up,1=Down,2=Left,3=Right): "))
        return action

    @staticmethod
    def random_action():
        """
        Returns a random action
        """
        return randrange(0, 4)

    @staticmethod
    def keyboard_from_gui(gui):
        """
        Returns the next key pressed (to be used only with the GUI)
        """
        while not gui.next_key.empty(): # This cleans all previously pressed keys
            gui.next_key.get()
        return gui.next_key.get() # Returns next pressed key
