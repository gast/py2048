"""
This file contains all necessary classes to launch the GUI for 2048
"""

import tkinter as tk
import threading
from queue import Queue
import time

SIZE = 500
GRID_LEN = 4
GRID_PADDING = 10

BACKGROUND_COLOR_GAME = "#92877d"
BACKGROUND_COLOR_CELL_EMPTY = "#9e948a"
BACKGROUND_COLOR_DICT = {2:"#eee4da", 4:"#ede0c8", 8:"#f2b179", 16:"#f59563", \
                        32:"#f67c5f", 64:"#f65e3b", 128:"#edcf72", 256:"#edcc61", \
                        512:"#edc850", 1024:"#edc53f", 2048:"#edc22e"}
CELL_COLOR_DICT = {2:"#776e65", 4:"#776e65", 8:"#f9f6f2", 16:"#f9f6f2", \
                    32:"#f9f6f2", 64:"#f9f6f2", 128:"#f9f6f2", 256:"#f9f6f2", \
                    512:"#f9f6f2", 1024:"#f9f6f2", 2048:"#f9f6f2"}
FONT = ("Verdana", 40, "bold")

KEY_UP_ALT = "\'\\uf700\'"
KEY_DOWN_ALT = "\'\\uf701\'"
KEY_LEFT_ALT = "\'\\uf702\'"
KEY_RIGHT_ALT = "\'\\uf703\'"

KEY_UP = "'w'"
KEY_DOWN = "'s'"
KEY_LEFT = "'a'"
KEY_RIGHT = "'d'"


# pylint: disable=too-many-ancestors
class RunGame(threading.Thread):
    """
    Classes needed to run the game in a thread and the graphics interface in another frame
    """
    def __init__(self, game, strategy, gui):
        self.game = game
        self.strategy = strategy
        threading.Thread.__init__(self)
        self.start()
        self.gui = gui
    def run(self):
        self.game.run_game(self.strategy)
        self.gui.update_grid_cells()

class GameGui(tk.Frame):
    """
    Associates the graphical interface to the game
    """
    def __init__(self, game):
        self.game = game
        self.size = game.size

        tk.Frame.__init__(self)

        self.grid()
        self.master.title('2048')
        self.master.bind("<Key>", self.key_down)

        self.commands = {KEY_UP: 0, KEY_DOWN: 1, KEY_LEFT: 2, KEY_RIGHT: 3,
                         KEY_UP_ALT: 0, KEY_DOWN_ALT: 1, KEY_LEFT_ALT: 2, KEY_RIGHT_ALT: 3}

        self.grid_cells = []
        self.init_grid()
        self.update_grid_cells()

        self.next_key = Queue()

    def run_game(self, strategy):
        """
        Lauch the game in a separate thread
        """
        RunGame(self.game, strategy, self)
        self.mainloop()


    def init_grid(self):
        """
        Initialize the graphical interface
        """
        background = tk.Frame(self, bg=BACKGROUND_COLOR_GAME, width=SIZE, height=SIZE)
        background.grid()
        for i in range(self.size):
            grid_row = []
            for j in range(self.size):
                cell = tk.Frame(background, bg=BACKGROUND_COLOR_CELL_EMPTY,
                                width=SIZE/self.size, height=SIZE/self.size)
                cell.grid(row=i, column=j, padx=GRID_PADDING, pady=GRID_PADDING)
                tk_grid = tk.Label(master=cell, text="", bg=BACKGROUND_COLOR_CELL_EMPTY,
                                   justify=tk.CENTER, font=FONT, width=4, height=2)
                tk_grid.grid()
                grid_row.append(tk_grid)

            self.grid_cells.append(grid_row)

    def update_grid_cells(self):
        """
        Refresh the display
        """
        matrix = self.game.board
        for i in range(self.size):
            for j in range(self.size):
                new_number = matrix[i][j]
                if new_number == 0:
                    self.grid_cells[i][j].configure(text="", bg=BACKGROUND_COLOR_CELL_EMPTY)
                else:
                    self.grid_cells[i][j].configure(text=str(new_number),
                                                    bg=BACKGROUND_COLOR_DICT[new_number],
                                                    fg=CELL_COLOR_DICT[new_number])
        if self.game.game_state() == 'win':
            self.grid_cells[1][1].configure(text="You", bg=BACKGROUND_COLOR_CELL_EMPTY)
            self.grid_cells[1][2].configure(text="Win!", bg=BACKGROUND_COLOR_CELL_EMPTY)
        if self.game.game_state() == 'lose':
            self.grid_cells[1][1].configure(text="You", bg=BACKGROUND_COLOR_CELL_EMPTY)
            self.grid_cells[1][2].configure(text="Lose!", bg=BACKGROUND_COLOR_CELL_EMPTY)
        self.update_idletasks()

    def pause_and_redisplay(self):
        """
        This function waits for a new key to be pressed (this is used to make pause
        in order to observer what a strategy does)
        """
        self.update_grid_cells()
        while not self.next_key.empty(): # This cleans all previously pressed keys
            self.next_key.get()
        self.next_key.get()
        self.update_grid_cells()

    def key_down(self, event):
        """
        Store the key in the queue "next_key" and refresh the display
        """
        key = repr(event.char)
        if key in self.commands:
            print(self.commands[repr(event.char)])
            self.next_key.put(self.commands[repr(event.char)])
            time.sleep(0.01)
            self.update_grid_cells()
