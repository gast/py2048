"""
Main file to launch the 2048 game
"""
import argparse
from game_engine import GameEngine
from game_gui import GameGui
from strategies import Strategies

def main():
    """
    This functions defines the way to parse argument and launches a new game
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--size", help="size of the board [n x n]", type=int)
    parser.add_argument("-s", "--strategy", type=int, choices=[0, 1, 2],
                        help="choice of strategy [0 = cli, 1 = random, 2 = gui], default=random")
    parser.add_argument("--gui", help="display the GUI", action="store_true")
    parser.add_argument("--visu", help="display the GUI and visualize the strategy by pausing",
                        action="store_true")

    args = parser.parse_args()
    size = (args.size if args.size else 4)

    strategy = (args.strategy if args.strategy is not None else 1)

    game = GameEngine(size, verbose=1)
    if args.gui or args.visu:
        game = GameGui(game)

    if strategy == 0:
        get_action = lambda board: Strategies.command_line_action()
    elif strategy == 1:
        get_action = lambda board: Strategies.random_action()
    elif strategy == 2:
        get_action = lambda board: Strategies.keyboard_from_gui(game)

    def apply_strategy(board):
        if args.visu:
            game.pause_and_redisplay()
            print('VISU!')
        return get_action(board)

    game.run_game(apply_strategy)

if __name__ == '__main__':
    main()
